<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use SlyFoxCreative\Bootstrap\Components\Button;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class ButtonTest extends TestCase
{
    public function testDefaultAttributeValues(): void
    {
        $button = new Button();

        self::assertSame(['class' => 'btn btn-primary'], $button->additionalAttributes());
        self::assertSame('button', $button->tag);
    }

    public function testAttributes(): void
    {
        $button = new Button('secondary', 'lg', true);

        self::assertSame(['class' => 'btn btn-secondary btn-lg'], $button->additionalAttributes());
        self::assertSame('a', $button->tag);
    }

    public function testRender(): void
    {
        $button = new Button();

        self::assertSame('bootstrap::button', $button->render()->name());
    }
}
