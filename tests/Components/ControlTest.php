<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use Illuminate\Support\HtmlString;
use SlyFoxCreative\Bootstrap\Components\Control;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class ControlTest extends TestCase
{
    public function testDefaultAdditionalAttributes(): void
    {
        $control = new Control('default');

        self::assertSame(['class' => 'has-validation'], $control->additionalAttributes());
    }

    public function testTypeAttribute(): void
    {
        $control = new Control('checkbox');

        self::assertSame(['class' => 'has-validation form-check'], $control->additionalAttributes());
    }

    public function testSwitchType(): void
    {
        $control = new Control('switch');

        self::assertSame(['class' => 'has-validation form-check form-switch'], $control->additionalAttributes());
    }

    public function testNameAttribute(): void
    {
        $control = new Control('checkbox', 'test');

        self::assertSame('test', $control->name);
    }

    public function testInlineAttribute(): void
    {
        $control = new Control('checkbox', 'test', true);

        self::assertSame(
            ['class' => 'has-validation form-check form-check-inline'],
            $control->additionalAttributes(),
        );
    }

    public function testLabel(): void
    {
        $control = new Control('checkbox');

        self::assertEquals(
            new HtmlString("<label class='form-check-label' for='test'>Test</label>"),
            $control->label('test'),
        );
    }

    public function testLabelWithIdSuffix(): void
    {
        $control = new Control('checkbox');

        self::assertEquals(
            new HtmlString("<label class='form-check-label' for='test_id'>Test</label>"),
            $control->label('test_id'),
        );
    }

    public function testRender(): void
    {
        $control = new Control('checkbox');

        self::assertSame('bootstrap::control', $control->render()->name());
    }
}
