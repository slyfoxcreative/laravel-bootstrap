<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use SlyFoxCreative\Bootstrap\Components\Col;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class ColTest extends TestCase
{
    public function testDefaultAttributeValues(): void
    {
        $col = new Col();

        self::assertSame(['class' => 'col'], $col->additionalAttributes());
        self::assertSame('div', $col->tag);
    }

    public function testAttributes(): void
    {
        $col = new Col('fieldset', 'auto', '1', '2', '3', '4', '5', '1', '2', '3', '4', '5', '6');

        self::assertSame(
            ['class' => 'col-auto col-xs-1 col-sm-2 col-md-3 col-lg-4 col-xl-5 offset-1 offset-xs-2 offset-sm-3 offset-md-4 offset-lg-5 offset-xl-6'],
            $col->additionalAttributes(),
        );
        self::assertSame('fieldset', $col->tag);
    }

    public function testRender(): void
    {
        $col = new Col();

        self::assertSame('bootstrap::col', $col->render()->name());
    }
}
