<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use SlyFoxCreative\Bootstrap\ServiceProvider as BootstrapServiceProvider;
use SlyFoxCreative\Html\ServiceProvider as HtmlServiceProvider;

class TestCase extends BaseTestCase
{
    protected function getPackageProviders($app)
    {
        return [
            BootstrapServiceProvider::class,
            HtmlServiceProvider::class,
        ];
    }
}
