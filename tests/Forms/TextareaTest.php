<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\textarea;

class TextareaTest extends TestCase
{
    public function testTextarea(): void
    {
        self::assertEquals(
            "<textarea class='form-control' id='test' name='test'>Test.</textarea>",
            textarea('test', 'Test.'),
        );
    }

    public function testTextareaClass(): void
    {
        self::assertEquals(
            "<textarea class='form-control test' id='test' name='test'></textarea>",
            textarea('test', '', ['class' => ['test']]),
        );
    }

    public function testTextareaWithError(): void
    {
        self::assertEquals(
            "<textarea class='form-control is-invalid' id='test' name='test'></textarea>",
            textarea('test', '', ['error' => true]),
        );
    }

    public function testTextareaWithNoText(): void
    {
        self::assertEquals(
            "<textarea class='form-control' id='test' name='test'></textarea>",
            textarea('test'),
        );
    }
}
