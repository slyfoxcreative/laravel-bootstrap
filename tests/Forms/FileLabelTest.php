<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\label;

class FileLabelTest extends TestCase
{
    public function testFileLabel(): void
    {
        self::assertEquals(
            "<label class='form-label' for='test'>Test</label>",
            label('test', 'Test', ['type' => 'file']),
        );
    }

    public function testFileLabelWithNoText(): void
    {
        self::assertEquals(
            "<label class='form-label' for='test'>Test</label>",
            label('test', attributes: ['type' => 'file']),
        );
    }
}
