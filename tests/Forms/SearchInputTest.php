<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\search;

class SearchInputTest extends TestCase
{
    public function testSearchInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='search'>",
            search('test'),
        );
    }
}
