<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\password;

class PasswordInputTest extends TestCase
{
    public function testPasswordInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='password'>",
            password('test'),
        );
    }
}
