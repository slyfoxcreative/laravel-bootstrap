<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\number;

class NumberInputTest extends TestCase
{
    public function testNumberInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='number'>",
            number('test'),
        );
    }
}
