<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\week;

class WeekInputTest extends TestCase
{
    public function testWeekInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='week'>",
            week('test'),
        );
    }
}
