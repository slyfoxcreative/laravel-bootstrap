<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\checkbox;

class CheckboxInputTest extends TestCase
{
    public function testCheckboxInput(): void
    {
        self::assertEquals(
            "<input class='form-check-input' id='test_1' name='test' type='checkbox' value='1'>",
            checkbox('test', ['value' => 1]),
        );
    }

    public function testClass(): void
    {
        self::assertEquals(
            "<input class='form-check-input test' id='test' name='test' type='checkbox' value='1'>",
            checkbox('test', ['class' => ['test']]),
        );
    }

    public function testDisabled(): void
    {
        self::assertEquals(
            "<input class='form-check-input' disabled id='test_1' name='test' type='checkbox' value='1'>",
            checkbox('test', ['disabled' => true, 'value' => 1]),
        );
    }
}
