<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\datetime_local;

class DatetimeLocalInputTest extends TestCase
{
    public function testDatetimeLocalInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='datetime-local'>",
            datetime_local('test'),
        );
    }
}
