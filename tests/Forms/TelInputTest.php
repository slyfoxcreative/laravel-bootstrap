<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\tel;

class TelInputTest extends TestCase
{
    public function testTelInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='tel'>",
            tel('test'),
        );
    }
}
