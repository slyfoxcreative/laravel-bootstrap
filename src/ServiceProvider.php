<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap;

use Composer\Semver\Comparator;
use Illuminate\Pagination\Paginator;
use Laravel\Dusk\Browser;
use SlyFoxCreative\Bootstrap\Components\Alert;
use SlyFoxCreative\Bootstrap\Components\Button;
use SlyFoxCreative\Bootstrap\Components\Col;
use SlyFoxCreative\Bootstrap\Components\Control;
use SlyFoxCreative\Bootstrap\Components\Modal;
use SlyFoxCreative\Bootstrap\Components\Navbar;
use SlyFoxCreative\Bootstrap\Components\Row;
use SlyFoxCreative\Html\AttributeModifierRegistry;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-bootstrap')
            ->hasViews()
            ->hasViewComponents(
                'bs',
                Alert::class,
                Button::class,
                Col::class,
                Control::class,
                Modal::class,
                Navbar::class,
                Row::class,
            )
        ;
    }

    public function packageBooted(): void
    {
        if (Comparator::greaterThanOrEqualTo(app()->version(), '8')) {
            Paginator::useBootstrap();
        }

        if (! app()->isProduction()) {
            Browser::macro('clickLabel', function ($for) {
                return $this->click("label[for={$for}]");
            });
        }

        $registry = resolve(AttributeModifierRegistry::class);
        $registry->add('SlyFoxCreative\Bootstrap\addClass');
        $registry->add('SlyFoxCreative\Bootstrap\addErrorAttributes', ['input', 'select', 'textarea']);
        $registry->add('SlyFoxCreative\Bootstrap\addInputClasses', ['input', 'textarea']);
        $registry->add('SlyFoxCreative\Bootstrap\addSelectClasses', ['select']);
        $registry->add('SlyFoxCreative\Bootstrap\addLabelClass', ['label']);
    }
}
