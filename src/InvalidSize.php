<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap;

class InvalidSize extends \Exception
{
    public function __construct(mixed $size)
    {
        $size = var_export($size, true);

        parent::__construct("Invalid form-control size {$size}");
    }
}
