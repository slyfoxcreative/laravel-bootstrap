<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap;

use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

use function SlyFoxCreative\Utilities\assert_string;
use function SlyFoxCreative\Utilities\is_instance_of;

/**
 * @param  AttributeCollection  $attributes
 * @param  array<string, mixed>  $context
 * @return AttributeCollection
 */
function addClass(Collection $attributes, array $context): Collection
{
    // If the class attribute doesn't exist, add it to the attributes Collection
    // as a Collection.
    if (! $attributes->has('class') || is_bool($attributes['class']) || is_null($attributes['class'])) {
        $class = new Collection();
    } else {
        $class = $attributes['class'];
    }

    // If it's an array, make it a Collection.
    if (is_array($class)) {
        $class = collect($class);
    }

    // Otherwise, it must be a string or a value that can be converted to a
    // string, so convert it ot a string and split it into a Collection.
    if (! is_object($class) || ! is_a($class, Collection::class)) {
        $class = collect(Str::of((string) $class)->split('/[\s,]+/'));
    }

    $attributes->put('class', $class);

    return $attributes;
}

/**
 * @param  AttributeCollection  $attributes
 * @param  array<string, mixed>  $context
 * @return AttributeCollection
 */
function addErrorAttributes(Collection $attributes, array $context): Collection
{
    /** @var AttributeValueCollection */
    $class = $attributes['class'];

    assert_string($context['name']);
    $name = $context['name'];

    $errors = session('errors');
    $error = $attributes['error'] ?? is_instance_of($errors, MessageBag::class) && $errors->has($name);

    $class->put('is-invalid', boolval($error));

    $attributes->forget('error');

    $attributes->put('class', $class);

    return $attributes;
}

/**
 * @param  AttributeCollection  $attributes
 * @param  array<string, mixed>  $context
 * @return AttributeCollection
 */
function addInputClasses(Collection $attributes, array $context): Collection
{
    /** @var AttributeValueCollection */
    $class = $attributes['class'];

    if ($attributes->has('size')) {
        if (! is_string($attributes['size']) || ! in_array($attributes['size'], ['sm', 'lg'], strict: true)) {
            throw new InvalidSize($attributes['size']);
        }

        $class->push("form-control-{$attributes['size']}");
        $attributes->forget('size');
    }

    switch ($attributes['type'] ?? null) {
        case 'checkbox':
        case 'radio':
            $class->push('form-check-input');

            break;

        case 'range':
            $class->push('form-range');

            break;

        default:
            $class->push('form-control');
    }

    $attributes->put('class', $class);

    return $attributes;
}

/**
 * @param  AttributeCollection  $attributes
 * @param  array<string, mixed>  $context
 * @return AttributeCollection
 */
function addSelectClasses(Collection $attributes, array $context): Collection
{
    /** @var AttributeValueCollection */
    $class = $attributes['class'];

    if ($attributes->has('size')) {
        if (! is_string($attributes['size']) || ! in_array($attributes['size'], ['sm', 'lg'], strict: true)) {
            throw new InvalidSize($attributes['size']);
        }

        $class->push("form-select-{$attributes['size']}");
        $attributes->forget('size');
    }

    $class->push('form-select');

    $attributes->put('class', $class);

    return $attributes;
}

/**
 * @param  AttributeCollection  $attributes
 * @param  array<string, mixed>  $context
 * @return AttributeCollection
 */
function addLabelClass(Collection $attributes, array $context): Collection
{
    /** @var AttributeValueCollection */
    $class = $attributes['class'];

    if (isset($attributes['type']) && $attributes['type'] === 'check') {
        $class->push('form-check-label');
    } else {
        $class->push('form-label');
    }

    $attributes->forget('type');

    $attributes->put('class', $class);

    return $attributes;
}
