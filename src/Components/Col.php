<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

/**
 * Bootstrap grid column component.
 *
 * Special attributes:
 *  - tag: the HTML tag to use for the column
 *  - size: default column span
 *  - size-[xs|sm|md|lg|xl]: column span for the given screen-size breakpoint
 *  - offset-[xs|sm|md|lg|xl]: column offset for the given screen-size
 *                             breakpoint
 */
class Col extends Component
{
    public string $tag;

    private ?string $size;

    private ?string $offset;

    /** @var Collection<string, ?string> */
    private Collection $sizes;

    /** @var Collection<string, ?string> */
    private Collection $offsets;

    public function __construct(
        string $tag = 'div',
        ?string $size = null,
        ?string $sizeXs = null,
        ?string $sizeSm = null,
        ?string $sizeMd = null,
        ?string $sizeLg = null,
        ?string $sizeXl = null,
        ?string $offset = null,
        ?string $offsetXs = null,
        ?string $offsetSm = null,
        ?string $offsetMd = null,
        ?string $offsetLg = null,
        ?string $offsetXl = null,
    ) {
        $this->tag = $tag;
        $this->size = $size;
        $this->sizes = collect([
            'xs' => $sizeXs,
            'sm' => $sizeSm,
            'md' => $sizeMd,
            'lg' => $sizeLg,
            'xl' => $sizeXl,
        ]);
        $this->offset = $offset;
        $this->offsets = collect([
            'xs' => $offsetXs,
            'sm' => $offsetSm,
            'md' => $offsetMd,
            'lg' => $offsetLg,
            'xl' => $offsetXl,
        ]);
    }

    /** @return array<string, string> */
    public function additionalAttributes(): array
    {
        $classes = collect();

        if (is_null($this->size) && $this->sizes->every(fn($x) => is_null($x))) {
            $classes->push('col');
        }

        if (! is_null($this->size)) {
            $classes->push("col-{$this->size}");
        }

        $this->sizes
            ->reject(fn($size) => is_null($size))
            ->each(fn($size, $name) => $classes->push("col-{$name}-{$size}"))
        ;

        if (! is_null($this->offset)) {
            $classes->push("offset-{$this->offset}");
        }

        $this->offsets
            ->reject(fn($offset) => is_null($offset))
            ->each(fn($offset, $name) => $classes->push("offset-{$name}-{$offset}"))
        ;

        return [
            'class' => $classes->join(' '),
        ];
    }

    public function render(): View
    {
        return view('bootstrap::col');
    }
}
