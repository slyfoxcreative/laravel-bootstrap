<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Illuminate\View\Component;

use function SlyFoxCreative\Html\label;

/**
 * Bootstrap control component. It renders the wrapper HTML for custom
 * checkboxes, radios and toggles.
 *
 * Special attributes:
 *  - type: checkbox, radio, switch
 *  - name: input name for rendering validation error messages
 *  - inline: set to true to render an inline control
 */
class Control extends Component
{
    public ?string $name;

    private string $type;

    private bool $inline;

    public function __construct(
        string $type = 'default',
        ?string $name = null,
        bool $inline = false,
    ) {
        $this->type = $type;
        $this->name = $name;
        $this->inline = $inline;
    }

    /**
     * Get additional attributes to be merged into the user-supplied
     * attributes.
     *
     * @return array<string, string>
     */
    public function additionalAttributes(): array
    {
        $class = new Collection([
            'has-validation',
        ]);

        if ($this->type !== 'default') {
            $class->push('form-check');
        }

        if ($this->type === 'switch') {
            $class->push('form-switch');
        }

        if ($this->inline) {
            $class->push('form-check-inline');
        }

        return [
            'class' => $class->join(' '),
        ];
    }

    /**
     * Create a custom-control label tag.
     *
     * @param  AttributeList  $attributes
     */
    public function label(string $id, ?string $text = null, array|Collection $attributes = []): HtmlString
    {
        if ($this->type !== 'default') {
            $attributes['type'] = 'check';
        }

        return label($id, $text, $attributes);
    }

    public function render(): View
    {
        return view('bootstrap::control');
    }
}
