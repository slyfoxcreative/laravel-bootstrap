<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

/**
 * HTML button component.
 *
 * Special attributes:
 *  - style: Bootstrap button style
 *  - size: Bootstrap size class
 *  - anchor: Whether to make the button an <a> instead of a <button>
 */
class Button extends Component
{
    public string $tag;

    private string $style;

    private ?string $size;

    public function __construct(
        string $style = 'primary',
        ?string $size = null,
        bool $anchor = false,
    ) {
        $this->style = $style;
        $this->size = $size;
        $this->tag = $anchor ? 'a' : 'button';
    }

    /**
     * Get additional attributes to be merged into the user-supplied
     * attributes.
     *
     * @return array<string, string>
     */
    public function additionalAttributes(): array
    {
        $attributes = [
            'class' => "btn btn-{$this->style}",
        ];

        if (! is_null($this->size)) {
            $attributes['class'] .= " btn-{$this->size}";
        }

        return $attributes;
    }

    public function render(): View
    {
        return view('bootstrap::button');
    }
}
