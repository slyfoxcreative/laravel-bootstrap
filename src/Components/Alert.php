<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

/**
 * Bootstrap alert component.
 *
 * Special attributes:
 *  - style: Bootstrap alert style
 *  - dismissible: whether to include a close button in the alert
 */
class Alert extends Component
{
    public bool $dismissible;

    private string $style;

    public function __construct(string $style = 'primary', bool $dismissible = false)
    {
        $this->style = $style;
        $this->dismissible = $dismissible;
    }

    /**
     * Get additional attributes to be merged into the user-supplied
     * attributes.
     *
     * @return array<string, string>
     */
    public function additionalAttributes(): array
    {
        $attributes = [
            'role' => 'alert',
            'class' => "alert alert-{$this->style}",
        ];

        if ($this->dismissible) {
            $attributes['class'] .= ' alert-dismissible fade show';
        }

        return $attributes;
    }

    public function render(): View
    {
        return view('bootstrap::alert');
    }
}
