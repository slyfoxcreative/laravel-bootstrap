<div {{ $attributes->class(['modal', 'fade'])->merge($additionalAttributes()) }}>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h2 id="{{ $id }}-label" class="modal-title fs-5">
          {{ $title }}
        </h2>
      </div>
      <div class="modal-body">
        {{ $body }}
      </div>
      <div class="modal-footer">
        {{ $actions }}
      </div>
    </div>
  </div>
</div>
