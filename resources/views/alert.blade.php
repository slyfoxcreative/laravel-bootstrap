<div {{ $attributes->merge($additionalAttributes()) }}>
  {{ $slot }}
  @if ($dismissible)
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  @endif
</div>
