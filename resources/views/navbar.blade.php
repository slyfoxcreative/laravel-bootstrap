<nav dusk="navbar" {{ $attributes->merge(['class' => "navbar navbar-expand-{$expandSize} navbar-{$style} bg-{$color} d-print-none"]) }}>
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ url('/') }}" dusk="brand-link">
      {{ $brand }}
    </a>
    @if ($collapsible)
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        {{ $slot }}
      </div>
    @else
      {{ $slot }}
    @endif
  </div>
</nav>
