<div {{ $attributes->merge($additionalAttributes()) }}>
  {{ $slot }}
  @if (isset($name))
    <div class="invalid-feedback">
      @error($name)
        {{ $message }}
      @enderror
    </div>
  @endif
</div>
